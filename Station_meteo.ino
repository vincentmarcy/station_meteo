// Exemple pour TBeam


#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

#define POPSCHOOL

//on charge le fichier config.h
#include "config.h"



WiFiMulti WiFiMulti;

//invite la bibliothèque DHT
#include "Adafruit_Sensor.h"
#include "DHT.h"

#define DHTPIN 25 //identifie la sortie du capteur
#define DHTTYPE DHT11 //identifie le type de capteur
//creer l'objet sensor de type DHT:
DHT sensor(DHTPIN, DHTTYPE);


float h, t; //declarer h:humidité et t:temperature comme variable décimal
int aff = 0; ///0: temp   1:humidity

//notre objet prévision meteo
StaticJsonDocument<1500> forecast;
StaticJsonDocument<400> actions;

void setup() //initialisation + connection wifi
{
  pinMode(4, OUTPUT);
  pinMode(22, OUTPUT);

  Serial.begin(115200);

  delay(10);

  // We start by connecting to a access point named "POP SENSORS", with password
  WiFiMulti.addAP(mySSID, myPASSWORD);
  // HOME WiFiMulti.addAP(SSID, PASSWORD);
  Serial.println();
  Serial.println();
  Serial.print("Waiting for WiFi... ");

  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  delay(500);


  sensor.begin();
}

void send_data(){ //sending data to NodeRed server
  HTTPClient http;
  Serial.println("============== sending ============");
  h = sensor.readHumidity();
  t = sensor.readTemperature();
  Serial.print("H:");
  Serial.print(h);
  Serial.print(",");
  Serial.print("T:");
  Serial.println(t);

  String host = "10.130.1.217:1880";

  //URL to get
  String url = "/sensor/g05?temp=" + String(t) + "&hygro=" + String(h) + "&label=Vince";
  http.begin("http://" + host + url);

  int retCode = http.GET();
  Serial.print("[HTTP] Return code =");
  Serial.println(retCode);
  if (retCode > 0) {
    //all is ok
    if (retCode == HTTP_CODE_OK) {
      digitalWrite(4, HIGH);
      digitalWrite(22, LOW);
      String content = http.getString();
      Serial.println(content);

    } else {
      digitalWrite (22, HIGH);
      digitalWrite (4, LOW);

    }
  } else {
    digitalWrite (22, HIGH);
    digitalWrite (4, LOW);
    // an error occured
    Serial.print ("[HTTP] Get failed, error :");
    Serial.println(retCode);
    http.end();
  }

}

void get_data() { //get data from OWM
  HTTPClient http;
  Serial.println("============== get ============");

  String host = "api.openweathermap.org";

  //URL to get
  //q= countrycode
  //cnt = count of results
  String url = "/data/2.5/forecast?q=willems,fr&units=metric&cnt=1&appid=" + String(myAPIKEY);
  http.begin("http://" + host + url);

  int retCode = http.GET();
  Serial.print("[HTTP] Return code =");
  Serial.println(retCode);
  if (retCode > 0) {
    //all is ok
    if (retCode == HTTP_CODE_OK) {
      digitalWrite(4, HIGH);
      digitalWrite(22, LOW);
      String content = http.getString();
      Serial.println(content);
      //lets try to parse it
      DeserializationError error = deserializeJson(forecast, content);
      // Test if parsing succeeds.
      if (error) {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.c_str());
        return;
      }
      //parsing was ok, grabbing values"
      float Wspeed = forecast["list"][0]["wind"]["speed"];
      float Wdir = forecast["list"][0]["wind"]["deg"];
      Serial.print("Wind speed (m/s)==>");
      Serial.println (Wspeed);
      Serial.print("Wind dir (°)==>");
      Serial.println (Wdir);
    } else {
      digitalWrite (22, HIGH);
      digitalWrite (4, LOW);

    }
  } else {
    digitalWrite (22, HIGH);
    digitalWrite (4, LOW);
    // an error occured
    Serial.print ("[HTTP] Get failed, error :");
    Serial.println(retCode);
    http.end();
  }

}

void get_actions() {
 HTTPClient http;
  //get action from nodered
  Serial.println("============== get_actions ============");
  //host to contact
  String host = "10.130.1.217:1880";

  //URL to get

  String url = "/get_actions";
  http.begin("http://" + host + url);

  int retCode = http.GET();

  Serial.print("[HTTP] Return code =");
  Serial.println(retCode);
  if (retCode > 0) {
    //all is ok
    if (retCode == HTTP_CODE_OK) {
      digitalWrite(4, HIGH);
      digitalWrite(22, LOW);
      String content = http.getString();
      Serial.println(content);
      //lets try to parse it
      DeserializationError error = deserializeJson(actions, content);
      // Test if parsing succeeds.
      if (error) {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.c_str());
        return;
      }
      //parsing was ok, grabbing values"

      if (actions["porte"] == 1) {
        Serial.println("OUVERTURE PORTE");
      }

      if (actions["porte"] == 0) {
        Serial.println("FERMETURE PORTE");
      }

    } else {
      digitalWrite (22, HIGH);
      digitalWrite (4, LOW);

    }
  } else {
    digitalWrite (22, HIGH);
    digitalWrite (4, LOW);
    // an error occured
    Serial.print ("[HTTP] Get failed, error :");
    Serial.println(retCode);
    http.end();
  }

}



void loop() // main loop

{
  send_data();
  delay(5000);
  get_data();
  delay(5000);
  get_actions();
  delay(5000);
}
